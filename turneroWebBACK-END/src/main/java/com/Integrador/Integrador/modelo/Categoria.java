package com.Integrador.Integrador.modelo;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class Categoria {
	@Id
	@GeneratedValue
	private Integer id;
	@Column
	private String nombre;
	@Column
	private String descripcion;
}

