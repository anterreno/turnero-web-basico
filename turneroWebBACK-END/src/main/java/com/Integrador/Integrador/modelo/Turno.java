package com.Integrador.Integrador.modelo;

import java.sql.Time;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;


import lombok.Data;

@Entity
@Data


public class Turno {
	@Id
	@GeneratedValue
	private Integer id;
	private String nombre;
	private String apellido;
	private Date fecha;
//	private Time hora;
	
	@ManyToOne
	private Categoria categoria;
	
}
