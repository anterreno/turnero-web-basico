package com.Integrador.Integrador.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.Integrador.Integrador.logica.CategoriaServicio;
import com.Integrador.Integrador.modelo.Categoria;
@RequestMapping	("/categorias")
@RestController
public class categoriaControlador {
		
	@Autowired
	private CategoriaServicio servicio;
		
	@GetMapping
	public List<Categoria> listarTodos() {
		return servicio.listarTodos();
	}
	@GetMapping(params = {"nombre" , "descripcion"})
	public Page<Categoria> ListarTodosFiltradoPorNombreYDescripcion(String nombre,String descripcion, Pageable pagina){
		return servicio.listarTodosFiltradoPorNombreYDescripcion(nombre, descripcion, pagina);
	}
	@PostMapping
	public Categoria guardar(@RequestBody Categoria c) {
		return servicio.guardar(c);
	}
		
	@RequestMapping(value = "/{id}" , method = RequestMethod.PUT)
	public Categoria actualizar(@RequestBody Categoria c, @PathVariable(name = "id") Integer id ) {
			return servicio.actualizar(c);
	}
	@RequestMapping(value ="/{id}", method = RequestMethod.DELETE)
	public void eliminar(@PathVariable(name="id") Integer id) {
		servicio.eliminar(id);
	}

}