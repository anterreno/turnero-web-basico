package com.Integrador.Integrador.web;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.Integrador.Integrador.logica.TurnoServicio;
import com.Integrador.Integrador.modelo.Turno;
@RestController

@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.DELETE})
@RequestMapping("/turnos")

public class turnoControlador {

	
	
	@Autowired
	private TurnoServicio servicio;

	@GetMapping
	public List<Turno> listarTodos(){
		return servicio.listarTodos();
	}
	@GetMapping(value= "/{id}")
	public Turno getTurno(@PathVariable(name = "id") Integer id){
		return servicio.getTurno(id);
	}
//	@GetMapping(params = {"hora"})
//	public Page<Turno> listarTodosFiltradoPorHora(Time hora, Pageable pagina) {
//		return servicio.listarTodosFiltradoPorHora(hora, pagina);
//	}
	@GetMapping(params = {"apellido"})
	public Page<Turno> listarTodosFiltradoPorApellido(String apellido, Pageable pagina) {
		return servicio.listarTodosFiltradoPorApellido(apellido, pagina);
	}
//	@GetMapping(params = {"fecha"})
//	public Page<Turno> listarTodosFiltradoPorFecha(java.sql.Date fecha, Pageable pagina) {
//		return servicio.listarTodosFiltradoPorFecha(fecha, pagina);
//	}
	@GetMapping(params = {"nombre"})
	public Page<Turno> listarTodosFiltradoPorNombre(String nombre, Pageable pagina) {
	return servicio.listarTodosFiltradoPorNombre(nombre, pagina);
	}
	@GetMapping(params = {"categoria"})
	public Page<Turno> listarTodosFiltradoPorCategoria(String categoria, Pageable pagina) {
	return servicio.listarTodosFiltradoPorCategoria(categoria, pagina);
	}
	@GetMapping(params= {"fecha"})
    public Page<Turno> listarTodosFiltradoPorFecha(@RequestParam("fecha") @DateTimeFormat(pattern = "dd/MM/yyyy") Date fecha, Pageable pagina) {
    return servicio.listarTodosFiltradoPorFecha(fecha, pagina);
    }
	@PostMapping
	public Turno guardar(@RequestBody Turno t) {
		
		if(servicio.listarFiltradoPorFecha(t.getFecha()) != null) {
			throw new RuntimeException("El turno esta Ocupado");
		}
		
		return servicio.guardar(t);
	}
	@CrossOrigin(origins = "*", methods= {RequestMethod.PUT})
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public Turno actualizar(@RequestBody Turno t, @PathVariable(name = "id") Integer id ) {

		return servicio.actualizar(t);
	}
	@RequestMapping(value ="/{id}", method = RequestMethod.DELETE)
	public void eliminar(@PathVariable(name="id") Integer id) {
				servicio.eliminar(id);
	}
	{


}
}

