package com.Integrador.Integrador.persistencia;


import java.sql.Time;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.Integrador.Integrador.modelo.Turno;

public interface turnoRepositorio extends JpaRepository<Turno, Integer> {

//	Page<Turno> findByHora(Time hora, Pageable pagina);
	Page<Turno> findByApellidoContainingIgnoreCase(String apellido, Pageable pagina);
	Page<Turno> findByFechaBetweenOrderByFecha(Date horaDesde, Date horaHasta, Pageable pagina);
	Page<Turno> findByNombreContainingIgnoreCase(String nombre, Pageable pagina);
	Page<Turno> findByCategoria_NombreContainingIgnoreCase(String categoria, Pageable pagina);
	Turno findByFecha(Date fecha);
	
}
