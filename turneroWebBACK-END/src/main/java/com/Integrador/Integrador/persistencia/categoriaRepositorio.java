package com.Integrador.Integrador.persistencia;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.Integrador.Integrador.modelo.Categoria;

@Repository
public interface categoriaRepositorio extends JpaRepository<Categoria, Integer>{
	Page<Categoria> findByNombreContainingIgnoreCaseAndDescripcionContainingIgnoreCase(String nombre, String descripcion, Pageable pagina);
	Page <Categoria> findByNombreContainingIgnoreCaseOrDescripcionContainingIgnoreCase(String nombre, String descripcion, Pageable pagina);
	
	@Query("select c from Categoria c where c.nombre like %?1% or c.descripcion like %?1%")
	List<Categoria> buscarPorNombreYDescripcion(String nombre, String descripcion);
}
