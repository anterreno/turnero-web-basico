package com.Integrador.Integrador.logica;


import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.Integrador.Integrador.modelo.Categoria;
import com.Integrador.Integrador.modelo.Turno;
import com.Integrador.Integrador.persistencia.turnoRepositorio;
@Service
public class TurnoServicio {

	@Autowired
	private turnoRepositorio repositorio;
	
	public Turno getTurno(Integer id) {
		return repositorio.findById(id).get();
	}

	
	
	public List<Turno> listarTodos(){
		Sort orden = Sort.by(Sort.Direction.ASC,"fecha");
		return repositorio.findAll(orden);
	}
//	public Page<Turno> listarTodosFiltradoPorHora(Time hora, Pageable pagina) {
//			return repositorio.findByHora(hora, pagina);
//	}
	public Turno guardar(Turno t) {
		Calendar fecha = Calendar.getInstance();
        Calendar horaDesde = Calendar.getInstance();
        Calendar horaHasta = Calendar.getInstance();
        Calendar fechaActual = Calendar.getInstance();
        
        //Sumamos 3 horas a la hora que se guarda ya que se guarda con 3 hs menos. 
        fecha.setTime(t.getFecha()); 
        fecha.add(Calendar.HOUR,3);
        
        //Le seteamos a horaDesde y horaHastal las horas limite
        horaDesde.set(fecha.get(Calendar.YEAR),fecha.get(Calendar.MONTH), fecha.get(Calendar.DAY_OF_MONTH), 8, 59, 59);
        horaHasta.set(fecha.get(Calendar.YEAR),fecha.get(Calendar.MONTH), fecha.get(Calendar.DAY_OF_MONTH), 17, 00, 00);
        fechaActual.set(fechaActual.get(Calendar.YEAR),fechaActual.get(Calendar.MONTH), fechaActual.get(Calendar.DAY_OF_MONTH));
        //Hacemos la validacion para ver si el turno se encuentra dentro de la franja horaria determinada
        if(fechaActual.compareTo(fecha) > 0) {
            throw new RuntimeException("No se puede asignar un turno para antes de la FECHA/HORARIO ACTUAL");
            }
        if(fecha.before(horaDesde) || fecha.after(horaHasta )) {
            throw new RuntimeException("El turno debe estar entre las 9:00 hs y las 17:00 hs");
        }
//        if(repositorio.findByFecha(fecha) != null ) {
//        	throw new RuntimeException("El turno seleccionado está ocupado. Por favor seleccione otro horario y/o fecha");
//        }
        
		System.out.println("Fecha: " + fecha.getTime());
        System.out.println("FechaDesde: " + horaDesde.getTime());
        System.out.println("FechaHasta: " + horaHasta.getTime());
        System.out.println("FechaActual: " + fechaActual.getTime());
		return repositorio.save(t);
        
		}
		
	public Turno actualizar(Turno t) {
		Calendar fecha = Calendar.getInstance();
        Calendar horaDesde = Calendar.getInstance();
        Calendar horaHasta = Calendar.getInstance();
        Calendar fechaActual = Calendar.getInstance();
        //Sumamos 3 horas a la hora que se guarda ya que se guarda con 3 hs menos. 
        
        fecha.setTime(t.getFecha()); 
        fecha.add(Calendar.HOUR,3);
        
        
        //Le seteamos a horaDesde y horaHastal las horas limite
        
        horaDesde.set(fecha.get(Calendar.YEAR),fecha.get(Calendar.MONTH), fecha.get(Calendar.DAY_OF_MONTH), 8, 59, 59);
        horaHasta.set(fecha.get(Calendar.YEAR),fecha.get(Calendar.MONTH), fecha.get(Calendar.DAY_OF_MONTH), 17, 00, 00);
        fechaActual.set(fechaActual.get(Calendar.YEAR),fechaActual.get(Calendar.MONTH), fechaActual.get(Calendar.DAY_OF_MONTH));
        
        //Hacemos la validacion para ver si el turno se encuentra dentro de la franja horaria determinada
        if(fechaActual.compareTo(fecha) > 0) {
            throw new RuntimeException("No se puede asignar un turno para antes de la FECHA ACTUAL");
            }
        if(fecha.before(horaDesde) || fecha.after(horaHasta )) {
            throw new RuntimeException("El turno debe estar entre las 9:00 hs y las 17:00 hs");
        }
		return repositorio.save(t);
}
	public void eliminar(Integer id) {
		Date fechaActual = new Date();
		Turno turno = repositorio.getOne(id);
		Date fechaTurno = turno.getFecha();
		System.out.println("FECHA TURNO: "+ fechaTurno.getTime());
		System.out.println("FECHA ACTUAL: "+ fechaActual.getTime());
		if(fechaTurno.compareTo(fechaActual) < 0) {
			throw new RuntimeException("El turno con id:" + id + " no se puede eliminar porque ya transcurrió.");
		}else {
			repositorio.deleteById(id);			
		}
	}

	public Page<Turno> listarTodosFiltradoPorApellido(String apellido, Pageable pagina) {
		return repositorio.findByApellidoContainingIgnoreCase(apellido, pagina);
	}

	public Page<Turno> listarTodosFiltradoPorNombre(String nombre, Pageable pagina){
		return repositorio.findByNombreContainingIgnoreCase(nombre, pagina);
		}
	public Page<Turno> listarTodosFiltradoPorFecha(Date fecha, Pageable pagina) {
		GregorianCalendar hasta = new GregorianCalendar();
		hasta.setTime(fecha);
		hasta.add(Calendar.DATE, 1);		
		return repositorio.findByFechaBetweenOrderByFecha(fecha, hasta.getTime(), pagina);
	}

	public Page<Turno> listarTodosFiltradoPorCategoria(String categoria, Pageable pagina){
		return repositorio.findByCategoria_NombreContainingIgnoreCase(categoria, pagina);
		}
	public Turno listarFiltradoPorFecha(Date fecha){
		return repositorio.findByFecha(fecha);
		}
	}
	
	
	
//	Page<Turno> findByNombreContainingIgnoreCase(String nombre, Pageable pagina);
//	Page<Turno> findByCategoria_NombreContainingIgnoreCase(String categoria, Pageable pagina);

