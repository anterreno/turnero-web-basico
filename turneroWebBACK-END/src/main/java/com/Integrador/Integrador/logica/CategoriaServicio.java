package com.Integrador.Integrador.logica;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.Integrador.Integrador.modelo.Categoria;
import com.Integrador.Integrador.persistencia.categoriaRepositorio;

@Service
public class CategoriaServicio {
	
	@Autowired		
	private categoriaRepositorio repositorio;
	
	public Categoria getCategoria(Integer id) {
		return repositorio.getOne(id);
	}
	public List<Categoria> listarTodos() {
		Sort orden =  Sort.by(Sort.Direction.ASC,"nombre","descripcion");
		return repositorio.findAll(orden);
	}
	public Categoria guardar(Categoria c ) {
		return repositorio.save(c);
	}
	public Categoria actualizar(Categoria c) {
		if(c.getId()== null) {
			throw new RuntimeException("Error el objeto no tiene id");
		}
		return repositorio.save(c);
	}
	public void eliminar(Integer id) {
		repositorio.deleteById(id);
	}
	public Page<Categoria> listarTodosFiltradoPorNombreYDescripcion(String nombre, String descripcion, Pageable pagina) {
		return repositorio.findByNombreContainingIgnoreCaseOrDescripcionContainingIgnoreCase(nombre, descripcion, pagina); 
		}
}