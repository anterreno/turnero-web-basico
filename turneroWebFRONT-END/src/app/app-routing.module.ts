import { TurnoComponent } from './components/turno/turno.component';
import { NuevoTurnoComponent } from './components/turnos/nuevo-turno/nuevo-turno.component';
import { TurnosComponent } from './components/turnos/turnos.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ModificarTurnoComponent } from './components/turnos/modificar-turno/modificar-turno.component';


const routes: Routes = [
  //{path: '', component: TurnosComponent},
  {path: 'turnos', component: TurnosComponent},
  {path: 'turnos/nuevo', component: NuevoTurnoComponent},
  {path: 'turnos/modificar/:id', component: ModificarTurnoComponent},
  {path: 'turnos/:id', component: TurnoComponent}
  //{path: '**', pathMatch: 'full', redirectTo : 'turnos'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
