
import { TurnoService } from './service/turno.service';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { DatePipe } from '@angular/common';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { TurnosComponent } from './components/turnos/turnos.component';
import { TurnoComponent } from './components/turno/turno.component';
import { NuevoTurnoComponent } from './components/turnos/nuevo-turno/nuevo-turno.component';
import { UrlService } from './service/url.service';
import { ModificarTurnoComponent } from './components/turnos/modificar-turno/modificar-turno.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    TurnosComponent,
    TurnoComponent,
    NuevoTurnoComponent,
    ModificarTurnoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [TurnoService, UrlService, DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
