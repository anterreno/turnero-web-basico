import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UrlService {

  private URL: string = "http://localhost:8080/";

  constructor() { }

  getURLBase(): string {
    return this.URL;
}
}
