import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { Injectable } from '@angular/core';
import { UrlService } from './url.service';

@Injectable({
  providedIn: 'root'
})
export class TurnoService {

  private turnos: Turno[] = [];
 // private URL_API: string= "https://reqres.in/api/users?page=1"
 // private URL_API_ID: string= "https://reqres.in/api/users/"

  constructor(private _http: HttpClient, private _url: UrlService) {

  }

  getTurnos(): Turno[] {
    return this.turnos;
  }

  getTurno(id: number): Observable<any> {
    return this._http.get<Turno[]>(this._url.getURLBase() + 'turnos/' + id);

  }

  get(): Observable<any> {
    return this._http.get<Turno[]>(this._url.getURLBase() + 'turnos');
  }

  getFiltradoPorNombre(txt: string): Observable<any>{
    return this._http.get<Turno[]>(this._url.getURLBase() + 'turnos?nombre=' + txt );
  }

  getFiltradoPorFecha(txt: string): Observable<any>{
    return this._http.get<Turno[]>(this._url.getURLBase() + 'turnos?fecha=' + txt );
  }

  guardarTurno(turno : any): Observable<any> {
    return this._http.post(this._url.getURLBase() + 'turnos', turno);
  }
  
  deleteTurno(id: number) {
    return this._http.delete(this._url.getURLBase() + 'turnos/' + id);
  }

  modificarTurno(turno: any,id: number): Observable<any> {
    const url = this._url.getURLBase() + 'turnos/' + id;
    console.log(url);
    return this._http.put<any>(this._url.getURLBase() + 'turnos/' + id, turno);

  }



}

export interface Turno {
  id: number;
  nombre: string;
  apellido: string;
  fecha: Date;
  categoria: string;
}
