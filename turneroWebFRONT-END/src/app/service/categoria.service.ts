import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { UrlService } from './url.service';

@Injectable({
  providedIn: 'root'
})
export class CategoriaService {

  constructor(private _http: HttpClient, private _url: UrlService) { }
  get(): Observable<any> {
    return this._http.get<Categoria[]>(this._url.getURLBase() + 'categorias');
  }
}
export interface Categoria {
  id: number;
  nombre: string;
}
