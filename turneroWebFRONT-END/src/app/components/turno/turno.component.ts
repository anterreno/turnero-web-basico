import { Turno } from './../../service/turno.service';
import { TurnoService } from 'src/app/service/turno.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-turno',
  templateUrl: './turno.component.html',
  styleUrls: ['./turno.component.css']
})
export class TurnoComponent implements OnInit {

  turno: any = [];
  constructor(private activate: ActivatedRoute,
    private servicioTurno: TurnoService) {
this.activate.params.subscribe((parametros) => {
this.turno = parametros['id'];
this.servicioTurno.getTurno(parametros['id']).subscribe(respuesta => {
  this.turno = respuesta;
})
});
}
ngOnInit(): void {
}

}
