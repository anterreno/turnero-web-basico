import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgForm } from '@angular/forms';
import * as $ from 'jquery';
import { Categoria, CategoriaService } from 'src/app/service/categoria.service';
import { TurnoService } from 'src/app/service/turno.service';


@Component({
  selector: 'app-modificar-turno',
  templateUrl: './modificar-turno.component.html',
  styleUrls: ['./modificar-turno.component.css']
})
export class ModificarTurnoComponent implements OnInit {
  turnos: any = {};
  categorias: Categoria[] = [];
  constructor(private activate: ActivatedRoute,
    private servicioTurno: TurnoService, private _servicioCategoria: CategoriaService, private _router : Router) {
    this.activate.params.subscribe(parametros => {
      this.servicioTurno.getTurno(parametros['id']).subscribe(respuesta => {
        this.turnos = respuesta;
        var hora = this.formatearHora(this.turnos.fecha);
        this.turnos.fecha = this.formatearFecha(this.turnos.fecha);
        $("select[id=hora] > option[value='" + hora + "'").prop("selected", true)
        console.log(this.turnos);
      });
    });
  }

  ngOnInit(): void {
    this._servicioCategoria.get().subscribe(respuesta => {
      this.categorias = respuesta;
    });

  }
  modificar(miFormulario: NgForm) {
    let turno: any = {};
    let id: number;
    turno.nombre = miFormulario.value.nombre;
    turno.apellido = miFormulario.value.apellido;
    turno.fecha = miFormulario.value.fecha + 'T' + miFormulario.value.hora + ':00.000-0300';
    turno.categoria = miFormulario.value.categoria;
    id = this.turnos.id;
    turno.id = this.turnos.id;
    if (miFormulario.valid) {
      this.servicioTurno.modificarTurno(turno, id).subscribe(respuesta => { console.log(respuesta) }, (err: any) => {
        alert(err.error.message);
        
      });
      alert('Modificación Exitosa!')
    } else {
      alert('Revise los campos');
    }
    this._router.navigate(['/turnos']);
  }
  compareCategoria(c1: Categoria, c2: Categoria): boolean {
    return c1 && c2 ? c1.id === c2.id : c1 === c2;
  }

  formatearFecha(fecha: string): string {
    let f = new Date(fecha);
    let dia = f.getDate().toString();
    let mes = (f.getMonth() + 1).toString();
    let anio = f.getFullYear().toString();
    if (dia.toString().length == 1) {
      dia = "0" + String(dia);
    }
    if (mes.toString().length == 1) {
      mes = "0" + String(mes);
    }
    return anio + "-" + mes + "-" + dia;
  }

  formatearHora(fecha: string): string {
    let h = new Date(fecha);
    let hora = h.getHours().toString();
    let min = h.getMinutes().toString();
    if (hora.toString().length == 1) {
      hora = "0" + String(hora);
    }
    if (min.toString().length == 1) {
      min = "0" + String(min);
    }
    return hora + ":" + min;
  }
}