import { Router } from '@angular/router';
import { TurnoService, Turno } from './../../service/turno.service';
import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-turnos',
  templateUrl: './turnos.component.html',
  styleUrls: ['./turnos.component.css']
})
export class TurnosComponent implements OnInit {

  turnos: Turno[] = [];
  txtBuscador: string;
  txtFecha: any;
  constructor(private _servicioTurno: TurnoService,
              private _router: Router,
              private _formatFecha: DatePipe) { }

  ngOnInit(): void {
    this._servicioTurno.get().subscribe(respuesta => {
      this.turnos = respuesta;
    });
    
  }

  verTurno(id: number) {
    this._router.navigate(['/turnos', id]);
  }

  buscar(){
    this._servicioTurno.getFiltradoPorNombre(this.txtBuscador).subscribe(respuesta => {
      this.turnos = respuesta.content;
    });
  }

  buscarPorFecha(){
    this._servicioTurno.getFiltradoPorFecha(this._formatFecha.transform(this.txtFecha, 'dd/MM/yyyy')).subscribe(respuesta => {
      
      this.turnos = respuesta.content;
    });
  }

  limpiarFiltros() {
    this.txtBuscador = undefined;
    this.txtFecha = undefined;
    this.recargar();

  }

  eliminarTurno(id: number){
    if(confirm('Está seguro que desea eliminar el turno?')){
      console.log(id);
       this._servicioTurno.deleteTurno(id).subscribe ( respuesta => {
        this.turnos = this.turnos.splice(1);


      });
      this.recargar();
    }
  }
  recargar(){
  window.location.reload();
  }
  
  modificarTurno(id: number) {
    this._router.navigate(['/turnos/modificar/' + id ]);
  }

}
