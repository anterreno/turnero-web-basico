import { TurnoService } from './../../../service/turno.service';
import { Component, OnInit } from '@angular/core';
import { CategoriaService, Categoria } from 'src/app/service/categoria.service';
import { TurnosComponent } from '../turnos.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nuevo-turno',
  templateUrl: './nuevo-turno.component.html',
  styleUrls: ['./nuevo-turno.component.css']
})
export class NuevoTurnoComponent implements OnInit {
  categorias: Categoria[] = [];
  constructor(private _servicio: TurnoService, private _servicioCategoria : CategoriaService, private _router: Router) { }

  ngOnInit(): void {
    this._servicioCategoria.get().subscribe(respuesta => {
      this.categorias = respuesta;
    });
  }

  guardar(miFormulario) {
     let turno : any ={};
    turno.apellido = miFormulario.value.apellido;
    turno.fecha = miFormulario.value.fecha + "T" + miFormulario.value.hora + ":00.000-0300"; 
    turno.nombre = miFormulario.value.nombre;
    turno.categoria = miFormulario.value.categoria;

    if (miFormulario.valid) {
      this. _servicio.guardarTurno(turno).subscribe( respuesta => {console.log(respuesta)});
      console.log(turno);
      alert('Carga Exitosa!')
    }else{
      alert('Revise los campos');
    }

  }

  recargar(){
    window.location.reload();
    }

}
